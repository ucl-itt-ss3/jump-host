<h1> Work agreement </h1>

### Members ###
- Aleksandra Slavova - alek2261@edu.eal.dk
- Geno Mitev - geno0010@edu.eal.dk

### Details of agreement: ###
-----------------------------
- The members agree to minimum work the hours given in the chapter “Details of agreed working hours”
- The members will work as according to the resource definition in the Plan for SS3 in Fronter.
- The members will agree with the other team members, either orally, or in writing, when they are present in the Academy and when they work at home on the project.
- The members will agree what kind of media will be used for sharing information, e.g. Google docs etc., and what rules to follow for sharing information.
- The members will notify each other in case of illness or other situations where a member is in a situation where he/she cannot attend the group activities in the Academy. 
This includes late appearance in the Academy (on-site) or when the group work off-site.
- In case a member is not fulfilling his/hers obligations it is up to the other members of the group to take actions as follows:
    - First occurrence of not fulfilling this working agreement, the member in question will be issued a written warning from the other group members.
    - Second occurrence of not fulfilling this working agreement, the member in question will called in for a meeting with the other group members!
    - Third occurrence of not fulfilling this working agreement, after having received first written warning and after having attended the meeting, the member in question will 
    be eligible for being expelled from the group. This is a decision made by the group! In case the member is expelled, the group must then consult the Executive who will then 
    contact the Student Counselor, and a decision will have to be made regarding the future activities of the expelled member.

### Details of agreed working hours ###
----------------------------------------

| *Day*   | *Start time* | *Break begins* | *Return to work* | *Finish time* | *Total* |
|---------|--------------|----------------|------------------|---------------|---------|
| Thursday|   8:15       |    11:30       |   12:15          |   14:30       |  5:30   |
| Friday  |   8:15       |    11:30       |   12:15          |   14:30       |  5:30   |
|         |              |                |                  | *Total hours* |  11:00  |