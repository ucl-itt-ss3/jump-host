#!/bin/bash
echo -e "\e[31mBash script which creates jump-account for the jump host!"
echo "------------------------"
read -p "Press enter to continue"
echo -e "Updating the system and installing the updates"
sudo apt-get update
sudo apt-get install
sudo apt install ssh
echo -e "\e[32mUpdated installed!"
echo "-------------------------"
echo -e "\e[31mCreating the new user jump-account!"
sudo useradd -m jump-account
echo -e "\e[32mUser created!"
