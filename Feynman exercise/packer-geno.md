# Packer

Packer is an open source tool for creating identical images for multiple platforms from a single source configuration. It is a lightweight and it runs on every major operating system. It is highly performant and it creates machine images for multiple platforms in parallel. We should use packer because its easy to use and automates the creation of any type of machine image. It embraces modern coniguration management by encouraging us to use different frameworks as Puppet to install and conifgure the software within our Packer-made images. Advantages of Packer are:
- Super fast and infrastructure deployment
  Packer images allow us to launch completely provisioned and configured machines in seconds, rather than several minutes or hours.
- Multi-provider portability
  Because Packer creates identical images for multiple platforms, you can run production in AWS, staging/QA in a private cloud like OpenStack, and development in desktop virtualization solutions such as VMware
- Improved stability
  Packer installs and configures all the software for a machine at the time the image is built. If there are bugs in these scripts, they’ll be caught early, rather than several minutes after a machine is launched.
- Greater testability
  After packer builds the machine image, that machine image can be quickly launched and tested to verify that things appear to be working.