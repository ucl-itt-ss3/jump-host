# Special Subject 3 - Jump host

### Network diagram

![network](network-diagram.PNG)

This is a personal project regarding making a Jump host and building a script for it. We have created a bash script which creates separate account called "jump-account"
which we will use for ssh connection. In order to deploy our Jump-host onto the ESXI we had to create a .json template which is executed by program called "Packer". 
When the .json file is runned with packer it creates an separate virtual machine and runs the bash script that we made. Then we  get the files from the created machine and deploy then onto the ESXI.

### Command to build the virutal machine with packer:
> packer build jump-host.json